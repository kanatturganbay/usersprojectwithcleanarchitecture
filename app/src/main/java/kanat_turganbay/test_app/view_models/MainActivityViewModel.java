package kanat_turganbay.test_app.view_models;

import android.annotation.SuppressLint;
import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import kanat_turganbay.test_app.R;
import kanat_turganbay.test_app.api_response.RetrofitClient;
import kanat_turganbay.test_app.api_response.RetrofitService;
import kanat_turganbay.test_app.constants.StringConstants;
import kanat_turganbay.test_app.models.AllHumanDatasModel;
import kanat_turganbay.test_app.models.EmploymentModel;
import kanat_turganbay.test_app.models.HumanDatasModel;
import kanat_turganbay.test_app.views.activities.MainActivity;
import kanat_turganbay.test_app.views.adapters.HumanDataAdapter;

import static kanat_turganbay.test_app.views.activities.MainActivity.TAG;

public class MainActivityViewModel extends AndroidViewModel {
    private HumanDataAdapter adapter;
    private boolean searched = false;
    private RetrofitService retrofitService;
    private AllHumanDatasModel allHumanDatasModel = new AllHumanDatasModel();
    private PublishSubject<HumanDatasModel> publishSubject = PublishSubject.create();
    private ArrayList<HumanDatasModel> searchedList = new ArrayList<>();
    private CompositeDisposable disposable = new CompositeDisposable();


    public MainActivityViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        setAllHumanDatasModel(new AllHumanDatasModel());
        setAdapter(new HumanDataAdapter(R.layout.item_card_layout, this));
        retrofitService = RetrofitClient.getClient().create(RetrofitService.class);
    }

    public AllHumanDatasModel getAllHumanDatasModel() {
        return allHumanDatasModel;
    }

    public void setAllHumanDatasModel(AllHumanDatasModel allHumanDatasModel) {
        this.allHumanDatasModel = allHumanDatasModel;
    }

    public HumanDataAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(HumanDataAdapter adapter) {
        this.adapter = adapter;
    }

    public void fetchData() {
        disposable.add(
                retrofitService
                        .fetchHumans()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(disposableObserver())
        );
    }

    public void fillAdapter(AllHumanDatasModel model) {
        Log.i(TAG, "MainActivityViewModel -> fillAdapter(model)");
        setSearched(false);
        getAdapter().updateList(model.getModels());
    }

    @SuppressLint("CheckResult")
    public void fillAdapter(final String search_input_string) {
        Log.i(TAG, "MainActivityViewModel -> fillAdapter(search_input_string)");

        Observable<ArrayList<HumanDatasModel>> observable = Observable.create(new ObservableOnSubscribe<ArrayList<HumanDatasModel>>() {
            @Override
            public void subscribe(ObservableEmitter<ArrayList<HumanDatasModel>> emitter) throws Exception {
                if (!emitter.isDisposed()) {
                    emitter.onNext(getAllHumanDatasModel().getSearchedModels(search_input_string));
                }
                if (!emitter.isDisposed()) {
                    emitter.onComplete();
                }
            }
        });

        observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<ArrayList<HumanDatasModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(ArrayList<HumanDatasModel> humanDatasModels) {
                        Log.i(TAG, "MainActivityViewModel -> fillAdapter(search_input_string) -> InsideObserver -> OnNext() -> humanDatasModels: " + humanDatasModels.toString());
                        setSearchedList(humanDatasModels);
                        getAdapter().updateList(humanDatasModels);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @SuppressLint("CheckResult")
    public void fillAdapterWithFilteredList(final String input_string_for_filtering) {
        Observable<ArrayList<HumanDatasModel>> observable = Observable.create(new ObservableOnSubscribe<ArrayList<HumanDatasModel>>() {
            @Override
            public void subscribe(ObservableEmitter<ArrayList<HumanDatasModel>> emitter) throws Exception {
                if (!emitter.isDisposed()) {
                    emitter.onNext(getAllHumanDatasModel().getFilteredModels(input_string_for_filtering));
                }

                if (!emitter.isDisposed()) {
                    emitter.onComplete();
                }
            }
        });

        observable
                .debounce(300, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<ArrayList<HumanDatasModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(ArrayList<HumanDatasModel> humanDatasModels) {
                        Log.i(TAG, "MainActivityViewModel -> fillAdapterWithFilteredList(input_string_for_filtering) -> InsideObserver -> OnNext() -> humanDatasModels: " + humanDatasModels);
                        setSearched(true);
                        setSearchedList(humanDatasModels);
                        getAdapter().updateList(humanDatasModels);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public boolean isSearched() {
        return searched;
    }

    public void setSearched(boolean searched) {
        this.searched = searched;
    }

    public HumanDatasModel getItem(Integer position) {
        if (isSearched()) {
            return getSearchedList().get(position);
        }
        return getAllHumanDatasModel().getModel(position);
    }

    public void onItemClicked(int position) {
        getPublishSubject().onNext(getItem(position));
    }

    public void onAllGenderRadioButtonClicked() {
        Log.i(TAG, "MainActivityViewModel -> onMaleGenderButtonClicked()");
        fillAdapter(getAllHumanDatasModel());
    }

    public void onMaleGenderButtonClicked() {
        Log.i(TAG, "MainActivityViewModel -> onMaleGenderButtonClicked()");
        fillAdapterWithFilteredList(getApplication().getString(R.string.male_string));
    }

    public void onFemaleGenderButtonClicked() {
        Log.i(TAG, "MainActivityViewModel -> onFemaleGenderButtonClicked()");
        fillAdapterWithFilteredList(getApplication().getString(R.string.female_string));
    }

    public PublishSubject<HumanDatasModel> getPublishSubject() {
        return publishSubject;
    }

    public void setPublishSubject(PublishSubject<HumanDatasModel> publishSubject) {
        this.publishSubject = publishSubject;
    }

    public ArrayList<HumanDatasModel> getModels() {
        ArrayList<HumanDatasModel> arrayList = new ArrayList<>();

        EmploymentModel employmentModel = new EmploymentModel();
        employmentModel.setName("Salam");
        employmentModel.setPosition("Mars");

        arrayList.add(new HumanDatasModel(1, "Ncvcvcvcvcvcvc", "SN", "E", "Male", "IP", "http://dummyimage.com/189x249.png/5fa2dd/ffffff", employmentModel));
        arrayList.add(new HumanDatasModel(1, "N", "SN", "E", "Female", "IP", "http://dummyimage.com/189x249.png/5fa2dd/ffffff", employmentModel));

        return arrayList;
    }

    public ArrayList<HumanDatasModel> getSearchedList() {
        return searchedList;
    }

    public void setSearchedList(ArrayList<HumanDatasModel> searchedList) {
        this.searchedList = searchedList;
    }

    private DisposableSingleObserver<List<HumanDatasModel>> disposableObserver() {
        return new DisposableSingleObserver<List<HumanDatasModel>>() {
            @Override
            public void onSuccess(List<HumanDatasModel> allHumanDatasModel) {
                Log.i(TAG, "MainActivityModel -> fetchData() -> disposableObserver() -> onSuccess() -> allHumanDatasModel: " + allHumanDatasModel);
                getAllHumanDatasModel().setModels((ArrayList<HumanDatasModel>) allHumanDatasModel);
                fillAdapter(getAllHumanDatasModel());
            }

            @Override
            public void onError(Throwable e) {
                Log.i(TAG, "MainActivityModel -> fetchData() -> disposableObserver() -> OnError() -> Error:  " + e.getLocalizedMessage());
            }
        };
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
