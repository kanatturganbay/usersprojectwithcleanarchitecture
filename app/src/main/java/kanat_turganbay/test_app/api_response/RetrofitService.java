package kanat_turganbay.test_app.api_response;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import kanat_turganbay.test_app.constants.StringConstants;
import kanat_turganbay.test_app.models.AllHumanDatasModel;
import kanat_turganbay.test_app.models.HumanDatasModel;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface RetrofitService {
    @GET(StringConstants.URL_CONTINUE)
    Single<List<HumanDatasModel>> fetchHumans();
}
