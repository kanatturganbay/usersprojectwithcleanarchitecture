package kanat_turganbay.test_app.utils;

import android.util.Log;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.Predicate;

import kanat_turganbay.test_app.models.HumanDatasModel;
import kanat_turganbay.test_app.views.activities.MainActivity;

public class FilteringArray {
    public static ArrayList<HumanDatasModel> searchFromArray(ArrayList<HumanDatasModel> arrayList, final String string) {
        Log.i(MainActivity.TAG, " FilteringArray -> searchFromArray() -> input_string: " + string);

        ArrayList<HumanDatasModel> humanDatasModels = new ArrayList<>();

        Log.i(MainActivity.TAG, " FilteringArray -> searchFromArray() filtering items: " + humanDatasModels);

        Iterator<HumanDatasModel> iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            HumanDatasModel model = iterator.next();

            if (model.getFirst_name().toLowerCase().contains(string) || model.getLast_name().toLowerCase().contains(string)) {
                humanDatasModels.add(model);
            }
        }
        return humanDatasModels;
    }

    public static ArrayList<HumanDatasModel> filterFromArray(ArrayList<HumanDatasModel> arrayList, final String string) {
        Log.i(MainActivity.TAG, " FilteringArray -> filterFromArray() -> input_string: " + string);

        ArrayList<HumanDatasModel> humanDatasModels = new ArrayList<>();

        Iterator<HumanDatasModel> iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            HumanDatasModel model = iterator.next();

            if (model.getGender().toLowerCase().equals(string)) {
                humanDatasModels.add(model);
            }
        }
        Log.i(MainActivity.TAG, " FilteringArray -> filterFromArray() filtered items: " + humanDatasModels);

        return humanDatasModels;
    }
}
