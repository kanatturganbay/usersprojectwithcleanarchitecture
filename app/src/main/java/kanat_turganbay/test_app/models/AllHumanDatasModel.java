package kanat_turganbay.test_app.models;

import android.annotation.TargetApi;
import android.os.Build;
import android.widget.Filterable;

import androidx.annotation.RequiresApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.function.Predicate;

import kanat_turganbay.test_app.utils.FilteringArray;

public class AllHumanDatasModel {

    private ArrayList<HumanDatasModel> models;

    public ArrayList<HumanDatasModel> getModels() {
        return models;
    }

    public void setModels(ArrayList<HumanDatasModel> models) {
        this.models = models;
    }

    public HumanDatasModel getModel(int position){
        return models.get(position);
    }

    public ArrayList<HumanDatasModel> getSearchedModels(String input_string){
        return FilteringArray.searchFromArray(getModels(), input_string.trim().toLowerCase());
    }

    public ArrayList<HumanDatasModel> getFilteredModels(String input_string){
        return FilteringArray.filterFromArray(getModels(), input_string.trim().toLowerCase());
    }

    @Override
    public String toString() {
        return "AllHumanDatasModel{" +
                "models=" + models +
                '}';
    }
}
