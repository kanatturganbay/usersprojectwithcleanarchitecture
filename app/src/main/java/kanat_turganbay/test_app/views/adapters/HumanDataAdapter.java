package kanat_turganbay.test_app.views.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.RecyclerView;

import com.jakewharton.rxbinding3.view.RxView;
import com.jakewharton.rxbinding3.widget.RxTextView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableConverter;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.subjects.PublishSubject;
import kanat_turganbay.test_app.R;
import kanat_turganbay.test_app.databinding.ItemCardLayoutBinding;
import kanat_turganbay.test_app.models.HumanDatasModel;
import kanat_turganbay.test_app.view_models.MainActivityViewModel;
import kotlin.Unit;

public class HumanDataAdapter extends RecyclerView.Adapter<HumanDataAdapter.ViewHolder> {

    private int layoutId;
    private ArrayList<HumanDatasModel> models = new ArrayList<>();
    private MainActivityViewModel viewModel;

    public HumanDataAdapter(@LayoutRes int layoutId, MainActivityViewModel viewModel) {
        this.layoutId = layoutId;
        this.viewModel = viewModel;
    }

    private int getLayoutIdForPosition(int position) {
        return layoutId;
    }

    @SuppressLint("CheckResult")
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemCardLayoutBinding binding = DataBindingUtil.inflate(inflater, viewType, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(viewModel, position);
    }

    @Override
    public int getItemViewType(int position) {
        return getLayoutIdForPosition(position);
    }

    @Override
    public int getItemCount() {
        return models == null ? 0 : models.size();
    }

    public void updateList(ArrayList<HumanDatasModel> models) {
        this.models.clear();
        this.models.addAll(models);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemCardLayoutBinding dataBinding;

        public ViewHolder(@NonNull ItemCardLayoutBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
        }

        public void bind(MainActivityViewModel viewModel, int position) {
            dataBinding.setViewModel(viewModel);
            dataBinding.setPosition(position);
            dataBinding.executePendingBindings();
        }
    }


}
