package kanat_turganbay.test_app.views.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.jakewharton.rxbinding3.view.RxView;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import kanat_turganbay.test_app.R;
import kanat_turganbay.test_app.databinding.ActivityMainBinding;
import kanat_turganbay.test_app.databinding.DialogFilterViewBinding;
import kanat_turganbay.test_app.databinding.DialogViewBinding;
import kanat_turganbay.test_app.models.AllHumanDatasModel;
import kanat_turganbay.test_app.models.HumanDatasModel;
import kanat_turganbay.test_app.view_models.MainActivityViewModel;
import kotlin.Unit;

public class MainActivity extends AppCompatActivity {
    MainActivityViewModel viewModel;
    CompositeDisposable disposable = new CompositeDisposable();
    public static final String TAG = "TestAppTag";
    ActivityMainBinding binding;
    PublishSubject<String> publisher = PublishSubject.create();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpBindings(savedInstanceState);
    }

    public void setUpBindings(Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        binding.setViewModel(viewModel);
        viewModel.init();
        setupList();
    }

    public void setupList() {
        viewModel.fetchData();

        binding.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                publisher.onNext(newText);
                return false;
            }
        });

        disposable.addAll(
                RxView
                        .clicks(binding.allGenderRadio)
                        .map(new Function<Unit, Integer>() {
                            @Override
                            public Integer apply(Unit unit) throws Exception {
                                return 0;
                            }
                        })
                        .subscribeWith(getDisposableObserver4()),

                RxView
                        .clicks(binding.maleGenderRadio)
                        .map(new Function<Unit, Integer>() {
                            @Override
                            public Integer apply(Unit unit) throws Exception {
                                return 1;
                            }
                        })
                        .subscribeWith(getDisposableObserver4()),
                RxView
                        .clicks(binding.femaleGenderRadio)
                        .map(new Function<Unit, Integer>() {
                            @Override
                            public Integer apply(Unit unit) throws Exception {
                                return 2;
                            }
                        })
                        .subscribeWith(getDisposableObserver4()),

                viewModel
                        .getPublishSubject()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(getDisposableObserver2()),

                publisher
                        .debounce(1000, TimeUnit.MILLISECONDS)
                        .distinctUntilChanged()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(getDisposableObserver3())
        );
    }


    public DisposableObserver<Integer> getDisposableObserver4() {
        return new DisposableObserver<Integer>() {
            @Override
            public void onNext(Integer integer) {
                switch (integer) {
                    case 0:
                        viewModel.onAllGenderRadioButtonClicked();
                        break;
                    case 1:
                        viewModel.onMaleGenderButtonClicked();
                        break;
                    case 2:
                        viewModel.onFemaleGenderButtonClicked();
                        break;
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
    }

    public DisposableObserver<String> getDisposableObserver3() {
        return new DisposableObserver<String>() {

            @Override
            public void onNext(String s) {
                Log.i(TAG, "MainActivity -> getDisposableObserver3 -> onNext() -> String: " + s);
                if (s.trim().isEmpty()) {
                    viewModel.setSearched(false);
                    viewModel.fillAdapter(viewModel.getAllHumanDatasModel());
                } else {
                    viewModel.setSearched(true);
                    viewModel.fillAdapter(s.trim());
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
    }

    public DisposableObserver<HumanDatasModel> getDisposableObserver2() {
        return new DisposableObserver<HumanDatasModel>() {
            @Override
            public void onNext(HumanDatasModel humanDatasModel) {
                showAlertDialog(humanDatasModel);
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        };
    }


    public void showAlertDialog(HumanDatasModel model) {
        DialogViewBinding viewBinding = DataBindingUtil
                .inflate(LayoutInflater.from(MainActivity.this), R.layout.dialog_view, null, false);

        String full_name = "Name: " + model.getFirst_name() + "\nSurname: " + model.getLast_name() + "\nGender: " + model.getGender() + "\nEmail: " + model.getEmail() + "\nEmplyment: " + model.getEmployment().getName() + "\nPosition at the job: " + model.getEmployment().getPosition();

        viewBinding.setImageUrl(model.getPhoto());
        viewBinding.setFullInfoText(full_name);

        Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(viewBinding.getRoot());
        dialog.setCancelable(true);

        dialog.show();
    }

    @Override
    protected void onDestroy() {
        disposable.clear();
        binding.unbind();
        super.onDestroy();
    }
}
